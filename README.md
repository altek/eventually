# Introduction
This package extends the [Eloquent](https://laravel.com/docs/eloquent) ORM by providing new events when interacting with pivot tables through the `toggle()`, `sync()`, `updateExistingPivot()`, `attach()` and `detach()` methods.

[![Packagist Version](https://img.shields.io/packagist/v/altek/eventually?style=for-the-badge)](https://packagist.org/packages/altek/eventually) [![Packagist Downloads](https://img.shields.io/packagist/dt/altek/eventually?style=for-the-badge)](https://packagist.org/packages/altek/eventually) [![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/altek/eventually/master?style=for-the-badge)](https://gitlab.com/altek/eventually/-/pipelines) [![Codecov](https://img.shields.io/codecov/c/gl/altek/eventually?style=for-the-badge)](https://codecov.io/gl/altek/eventually) [![Packagist License](https://img.shields.io/packagist/l/altek/eventually?style=for-the-badge)](https://packagist.org/packages/altek/eventually)

## Version Matrix
 Version   | Illuminate      | Status | PHP Version
-----------|-----------------|--------|-------------
 3.x       | 10.x.y - 12.x.y | Latest | >= 8.1.0
 2.x       | 8.x.y - 10.x.y  | EOL    | >= 7.4.0
 1.x       | 5.5.x - 7.x.y   | EOL    | >= 7.1.3

## Documentation
The package documentation can be found [here](https://altek.gitlab.io/eventually/).

## Changelog
For information on recent changes, check the [CHANGELOG](CHANGELOG.md).

## Contributing
Contributions are always welcome, but before anything else, make sure you get acquainted with the [CONTRIBUTING](CONTRIBUTING.md) guide.

### Security
If you find a security related issue, please email security@altek.org instead of using the issue tracker.

## Credits
- [Quetzy Garcia](https://gitlab.com/quetzyg)

## License
The **Eventually** package is open source software licensed under the [MIT LICENSE](LICENSE.md).
