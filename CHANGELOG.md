# Changelog
Relevant changes to the Eventually package will be documented here.

## v3.0.2 (2025-02-26)
### Added
- `illuminate/database` v12.x support

## v3.0.1 (2025-01-20)
## Fixed
- Update coding style to prevent deprecation warnings in PHP 8.4 (fixes [#2](https://gitlab.com/altek/eventually/issues/2)), by [Philipp Nardmann](https://gitlab.com/pnardmann)

## v3.0.0 (2024-03-21)
### Added
- `PHP` v8.3 support by [Julien Arcin](https://gitlab.com/julienarcin)
- `illuminate/database` v11.x support [Julien Arcin](https://gitlab.com/julienarcin)

### Removed
- `PHP` v7.4 and v8.0 support
- `illuminate/database` v8.x and v9.x support

## v2.0.3 (2023-03-15)
### Added
- Support `illuminate/database` v10.x

### Removed
- `PHP` v7.3 support

## v2.0.2 (2022-02-12)
### Added
- Support `illuminate/database` v9.x

## v2.0.1 (2020-12-01)
### Added
- Support PHP 8.x

## v2.0.0 (2020-09-09)
### Added
- Support `illuminate/database` v8.x

## v1.0.9 (2020-03-03)
### Added
- Support `illuminate/database` v7.x

## v1.0.8 (2020-02-18)
## Fixed
- Wrong JSON structure when passing a `\Illuminate\Support\Collection` to sync() ([#2](https://gitlab.com/altek/eventually/issues/2))

## v1.0.7 (2019-10-09)
### Added
- Support `illuminate/database` v6.2

## v1.0.6 (2019-10-07)
### Added
- Support `illuminate/database` v6.1

## v1.0.5 (2019-09-03)
### Added
- Support `illuminate/database` v6.0

## v1.0.4 (2019-02-26)
### Added
- Support `illuminate/database` v5.8

## v1.0.3 (2019-02-13)
## Fixed
- Use the correct foreign/related pivot key values

## v1.0.2 (2019-02-05)
### Added
- Code optimisations

## Fixed
- Ambiguous id column in query when detaching all relations ([#1](https://gitlab.com/altek/eventually/issues/1))

## v1.0.1 (2019-01-02)
### Removed
- `illuminate/database` v5.4 support

## v1.0.0 (2019-01-01)

- Initial release
