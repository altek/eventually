<?php

declare(strict_types=1);

namespace Altek\Eventually\Tests\Integration\MorphToMany;

use Altek\Eventually\Tests\Database\Factories\AwardFactory;
use Altek\Eventually\Tests\Database\Factories\UserFactory;
use Altek\Eventually\Tests\EventuallyTestCase;
use Altek\Eventually\Tests\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection as BaseCollection;
use Illuminate\Support\Facades\Event;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;

class AttachTest extends EventuallyTestCase
{
    /**
     * @return array
     */
    public static function attachProvider(): array
    {
        return [
            [
                // Id
                1,

                // Attributes
                [],

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 1,
                        ],
                    ],
                ],
            ],

            [
                // Id
                [
                    2,
                ],

                // Attributes
                [
                    'prize' => 1024,
                ],

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'prize'          => 1024,
                            'award_id'       => 2,
                        ],
                    ],
                ],
            ],

            [
                // Id
                [
                    2 => [
                        'prize' => 4096,
                    ],
                    1 => [
                        'prize' => 8192,
                    ],
                ],

                // Attributes
                [],

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'prize'          => 4096,
                            'award_id'       => 2,
                        ],
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'prize'          => 8192,
                            'award_id'       => 1,
                        ],
                    ],
                ],
            ],

            [
                // Id
                Model::class,

                // Attributes
                [],

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 1,
                        ],
                    ],
                ],
            ],

            [
                // Id
                Collection::class,

                // Attributes
                [
                    'prize' => 512,
                ],

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'prize'          => 512,
                            'award_id'       => 1,
                        ],
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'prize'          => 512,
                            'award_id'       => 2,
                        ],
                    ],
                ],
            ],

            [
                // Id
                BaseCollection::make(1),

                // Attributes
                [],

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 1,
                        ],
                    ],
                ],
            ],

            [
                // Id
                BaseCollection::make([
                    2,
                    1,
                ]),

                // Attributes
                [],

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 2,

                        ],
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 1,
                        ],
                    ],
                ],
            ],
        ];
    }

    #[Test]
    public function itSuccessfullyRegistersEventListeners(): void
    {
        User::attaching(static function ($user, $relation, $properties): void {
            self::assertInstanceOf(User::class, $user);

            self::assertSame('awards', $relation);

            self::assertSame([
                [
                    'awardable_id'   => 1,
                    'awardable_type' => User::class,
                    'prize'          => 1024,
                    'award_id'       => 1,
                ],
                [
                    'awardable_id'   => 1,
                    'awardable_type' => User::class,
                    'prize'          => 1024,
                    'award_id'       => 2,
                ],
            ], $properties);
        });

        User::attached(static function ($user, $relation, $properties): void {
            self::assertInstanceOf(User::class, $user);

            self::assertSame('awards', $relation);

            self::assertSame([
                [
                    'awardable_id'   => 1,
                    'awardable_type' => User::class,
                    'prize'          => 1024,
                    'award_id'       => 1,
                ],
                [
                    'awardable_id'   => 1,
                    'awardable_type' => User::class,
                    'prize'          => 1024,
                    'award_id'       => 2,
                ],
            ], $properties);
        });

        $user   = UserFactory::new()->create();
        $awards = AwardFactory::new()->count(2)->create();

        self::assertCount(0, $user->awards()->get());

        self::assertTrue($user->awards()->attach($awards, [
            'prize' => 1024,
        ]));

        self::assertCount(2, $user->awards()->get());
    }

    #[Test]
    public function itPreventsModelsFromBeingAttached(): void
    {
        User::attaching(static function () {
            return false;
        });

        $user   = UserFactory::new()->create();
        $awards = AwardFactory::new()->count(2)->create();

        self::assertCount(0, $user->awards()->get());

        self::assertFalse($user->awards()->attach($awards));

        self::assertCount(0, $user->awards()->get());
    }

    /**
     * @param mixed $id
     * @param array $attributes
     * @param array $expectedPayload
     */
    #[Test]
    #[DataProvider('attachProvider')]
    public function itSuccessfullyAttachesModels($id, array $attributes, array $expectedPayload): void
    {
        $user   = UserFactory::new()->create();
        $awards = AwardFactory::new()->count(2)->create();

        self::assertCount(0, $user->awards()->get());

        Event::fake();

        switch ($id) {
            case Model::class:
                $id = $awards->first();
                break;

            case Collection::class:
                $id = $awards;
                break;
        }

        self::assertTrue($user->awards()->attach($id, $attributes));

        Event::assertDispatched(sprintf('eloquent.attaching: %s', User::class), static function ($event, $payload, $halt) use ($expectedPayload) {
            self::assertInstanceOf(User::class, $payload[0]);

            unset($payload[0]);

            self::assertSame($expectedPayload, $payload);

            self::assertTrue($halt);

            return true;
        });

        Event::assertDispatched(sprintf('eloquent.attached: %s', User::class), static function ($event, $payload) use ($expectedPayload) {
            self::assertInstanceOf(User::class, $payload[0]);

            unset($payload[0]);

            self::assertSame($expectedPayload, $payload);

            return true;
        });
    }
}
