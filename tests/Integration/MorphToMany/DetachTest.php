<?php

declare(strict_types=1);

namespace Altek\Eventually\Tests\Integration\MorphToMany;

use Altek\Eventually\Tests\Database\Factories\AwardFactory;
use Altek\Eventually\Tests\Database\Factories\UserFactory;
use Altek\Eventually\Tests\EventuallyTestCase;
use Altek\Eventually\Tests\Models\Award;
use Altek\Eventually\Tests\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection as BaseCollection;
use Illuminate\Support\Facades\Event;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;

class DetachTest extends EventuallyTestCase
{
    /**
     * @return array
     */
    public static function detachProvider(): array
    {
        return [
            [
                // Results
                2,

                // Id
                null,

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 1,
                        ],
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 2,
                        ],
                    ],
                ],
            ],

            [
                // Results
                1,

                // Id
                1,

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                1,

                // Id
                [
                    2,
                ],

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 2,
                        ],
                    ],
                ],
            ],

            [
                // Results
                2,

                // Id
                [
                    2,
                    1,
                ],

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 2,
                        ],
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                1,

                // Id
                Model::class,

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                2,

                // Id
                Collection::class,

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 1,
                        ],
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 2,
                        ],
                    ],
                ],
            ],

            [
                // Results
                1,

                // Id
                BaseCollection::make(1),

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                2,

                // Id
                BaseCollection::make([
                    2,
                    1,
                ]),

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 2,
                        ],
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 1,
                        ],
                    ],
                ],
            ],
        ];
    }

    #[Test]
    public function itSuccessfullyRegistersEventListeners(): void
    {
        User::detaching(static function ($user, $relation, $properties): void {
            self::assertInstanceOf(User::class, $user);

            self::assertSame('awards', $relation);

            self::assertSame([
                [
                    'awardable_id'   => 1,
                    'awardable_type' => User::class,
                    'award_id'       => 1,
                ],
                [
                    'awardable_id'   => 1,
                    'awardable_type' => User::class,
                    'award_id'       => 2,
                ],
            ], $properties);
        });

        User::detached(static function ($user, $relation, $properties): void {
            self::assertInstanceOf(User::class, $user);

            self::assertSame('awards', $relation);

            self::assertSame([
                [
                    'awardable_id'   => 1,
                    'awardable_type' => User::class,
                    'award_id'       => 1,
                ],
                [
                    'awardable_id'   => 1,
                    'awardable_type' => User::class,
                    'award_id'       => 2,
                ],
            ], $properties);
        });

        $user = UserFactory::new()->create();

        $awards = AwardFactory::new()->count(2)->create()->each(static function (Award $award) use ($user): void {
            $award->users()->attach($user);
        });

        self::assertCount(2, $user->awards()->get());

        self::assertSame(2, $user->awards()->detach($awards));

        self::assertCount(0, $user->awards()->get());
    }

    #[Test]
    public function itPreventsModelsFromBeingDetached(): void
    {
        User::detaching(static function () {
            return false;
        });

        $user = UserFactory::new()->create();

        $awards = AwardFactory::new()->count(2)->create()->each(static function (Award $award) use ($user): void {
            $award->users()->attach($user);
        });

        self::assertCount(2, $user->awards()->get());

        self::assertFalse($user->awards()->detach($awards));

        self::assertCount(2, $user->awards()->get());
    }

    /**
     * @param int   $results
     * @param mixed $id
     * @param array $expectedPayload
     */
    #[Test]
    #[DataProvider('detachProvider')]
    public function itSuccessfullyDetachesModels(int $results, $id, array $expectedPayload): void
    {
        $user = UserFactory::new()->create();

        $awards = AwardFactory::new()->count(2)->create()->each(static function (Award $award) use ($user): void {
            $award->users()->attach($user, [
                'prize' => 8192,
            ]);
        });

        self::assertCount(2, $user->awards()->get());

        Event::fake();

        switch ($id) {
            case Model::class:
                $id = $awards->first();
                break;

            case Collection::class:
                $id = $awards;
                break;
        }

        self::assertSame($results, $user->awards()->detach($id));

        self::assertCount(2 - $results, $user->awards()->get());

        Event::assertDispatched(sprintf('eloquent.detaching: %s', User::class), static function ($event, $payload, $halt) use ($expectedPayload) {
            self::assertInstanceOf(User::class, $payload[0]);

            unset($payload[0]);

            self::assertSame($expectedPayload, $payload);

            self::assertTrue($halt);

            return true;
        });

        Event::assertDispatched(sprintf('eloquent.detached: %s', User::class), static function ($event, $payload) use ($expectedPayload) {
            self::assertInstanceOf(User::class, $payload[0]);

            unset($payload[0]);

            self::assertSame($expectedPayload, $payload);

            return true;
        });
    }
}
