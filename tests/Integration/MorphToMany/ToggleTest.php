<?php

declare(strict_types=1);

namespace Altek\Eventually\Tests\Integration\MorphToMany;

use Altek\Eventually\Tests\Database\Factories\AwardFactory;
use Altek\Eventually\Tests\Database\Factories\UserFactory;
use Altek\Eventually\Tests\EventuallyTestCase;
use Altek\Eventually\Tests\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection as BaseCollection;
use Illuminate\Support\Facades\Event;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;

class ToggleTest extends EventuallyTestCase
{
    /**
     * @return array
     */
    public static function toggleProvider(): array
    {
        return [
            [
                // Results
                [
                    'attached' => [
                        1,
                    ],
                    'detached' => [],
                ],

                // Id
                1,

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                [
                    'attached' => [
                        2,
                    ],
                    'detached' => [],
                ],

                // Id
                [
                    2,
                ],

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 2,
                        ],
                    ],
                ],
            ],

            [
                // Results
                [
                    'attached' => [
                        2,
                        1,
                    ],
                    'detached' => [],
                ],

                // Id
                [
                    2,
                    1,
                ],

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 2,
                        ],
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                [
                    'attached' => [
                        1,
                    ],
                    'detached' => [],
                ],

                // Id
                Model::class,

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                [
                    'attached' => [
                        1,
                        2,
                    ],
                    'detached' => [],
                ],

                // Id
                Collection::class,

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 1,
                        ],
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 2,
                        ],
                    ],
                ],
            ],

            [
                // Results
                [
                    'attached' => [
                        1,
                    ],
                    'detached' => [],
                ],

                // Id
                BaseCollection::make(1),

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 1,
                        ],
                    ],
                ],
            ],

            [
                // Results
                [
                    'attached' => [
                        2,
                        1,
                    ],
                    'detached' => [],
                ],

                // Id
                BaseCollection::make([
                    2,
                    1,
                ]),

                // Expected payload
                [
                    1 => 'awards',
                    2 => [
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 2,
                        ],
                        [
                            'awardable_id'   => 1,
                            'awardable_type' => User::class,
                            'award_id'       => 1,
                        ],
                    ],
                ],
            ],
        ];
    }

    #[Test]
    public function itSuccessfullyRegistersEventListeners(): void
    {
        User::toggling(static function ($user, $relation, $properties): void {
            self::assertInstanceOf(User::class, $user);

            self::assertSame('awards', $relation);

            self::assertSame([
                [
                    'awardable_id'   => 1,
                    'awardable_type' => User::class,
                    'award_id'       => 1,
                ],
            ], $properties);
        });

        User::toggled(static function ($user, $relation, $properties): void {
            self::assertInstanceOf(User::class, $user);

            self::assertSame('awards', $relation);

            self::assertSame([
                [
                    'awardable_id'   => 1,
                    'awardable_type' => User::class,
                    'award_id'       => 1,
                ],
            ], $properties);
        });

        $user  = UserFactory::new()->create();
        $award = AwardFactory::new()->create();

        self::assertCount(0, $user->awards()->get());

        self::assertSame([
            'attached' => [
                1,
            ],
            'detached' => [],
        ], $user->awards()->toggle($award));

        self::assertCount(1, $user->awards()->get());
    }

    #[Test]
    public function itPreventsModelsFromBeingToggled(): void
    {
        User::toggling(static function () {
            return false;
        });

        $user   = UserFactory::new()->create();
        $awards = AwardFactory::new()->count(2)->create();

        self::assertCount(0, $user->awards()->get());

        self::assertFalse($user->awards()->toggle($awards));

        self::assertCount(0, $user->awards()->get());
    }

    /**
     * @param array $results
     * @param mixed $id
     * @param array $expectedPayload
     */
    #[Test]
    #[DataProvider('toggleProvider')]
    public function itSuccessfullyTogglesModels(array $results, $id, array $expectedPayload): void
    {
        $user   = UserFactory::new()->create();
        $awards = AwardFactory::new()->count(2)->create();

        self::assertCount(0, $user->awards()->get());

        Event::fake();

        switch ($id) {
            case Model::class:
                $id = $awards->first();
                break;

            case Collection::class:
                $id = $awards;
                break;
        }

        self::assertSame($results, $user->awards()->toggle($id));

        Event::assertDispatched(sprintf('eloquent.toggling: %s', User::class), static function ($event, $payload, $halt) use ($expectedPayload) {
            self::assertInstanceOf(User::class, $payload[0]);

            unset($payload[0]);

            self::assertSame($expectedPayload, $payload);

            self::assertTrue($halt);

            return true;
        });

        Event::assertDispatched(sprintf('eloquent.toggled: %s', User::class), static function ($event, $payload) use ($expectedPayload) {
            self::assertInstanceOf(User::class, $payload[0]);

            unset($payload[0]);

            self::assertSame($expectedPayload, $payload);

            return true;
        });
    }
}
