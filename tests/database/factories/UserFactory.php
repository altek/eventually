<?php

declare(strict_types=1);

namespace Altek\Eventually\Tests\Database\Factories;

use Altek\Eventually\Tests\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * {@inheritDoc}
     */
    protected $model = User::class;

    /**
     * {@inheritDoc}
     */
    public function definition(): array
    {
        return [
            'is_admin'   => $this->faker->boolean(),
            'first_name' => $this->faker->firstName,
            'last_name'  => $this->faker->lastName,
            'email'      => $this->faker->unique()->safeEmail,
        ];
    }
}
