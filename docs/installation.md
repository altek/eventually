# Installation
To get started with Eventually, use [Composer](https://getcomposer.org/doc/00-intro.md) to include the package to your dependencies.

The latest version brings support for [Laravel](https://laravel.com/docs/) & [Lumen](https://lumen.laravel.com/docs/) **10.0** and up.

To install the package, execute the following command on your project root:
```sh
composer require altek/eventually
```

## Version matrix
 Version   | Illuminate      | Status | PHP Version
-----------|-----------------|--------|-------------
 3.x       | 10.x.y - 12.x.y | Latest | >= 8.1.0
 2.x       | 8.x.y - 10.x.y  | EOL    | >= 7.4.0
 1.x       | 5.5.x - 7.x.y   | EOL    | >= 7.1.3

# Model setup
By using the `Altek\Eventually\Eventually` trait on an Eloquent model, the new pivot events become available.

## Example
```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use \Altek\Eventually\Eventually;

    // ...
}
```

From this point on, any `BelongsToMany` or `MorphToMany` relation will fire the corresponding [events](events.md) when using the `toggle()`, `sync()`, `updateExistingPivot()`, `attach()` or `detach()` method.
